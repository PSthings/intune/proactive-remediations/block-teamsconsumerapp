# Remediation

try {

    if ((Test-Path -LiteralPath "HKLM:\Software\Microsoft\Windows\CurrentVersion\Communications") -ne $true) {
        New-Item "HKLM:\Software\Microsoft\Windows\CurrentVersion\Communications" -Force -ErrorAction Stop 
    }
    New-ItemProperty -LiteralPath 'HKLM:\Software\Microsoft\Windows\CurrentVersion\Communications' -Name 'ConfigureChatAutoInstall' -Value 0 -PropertyType DWord -Force -ErrorAction Stop
    

    # uninstall the teams consumer app
    Get-AppxPackage -Name MicrosoftTeams | Remove-AppxPackage -ErrorAction stop

    # as nothing errored out, we will report success
    Write-Host "Success, regkey set and app uninstalled"
    exit 0
}

catch {
    $errMsg = _.Exception.Message
    Write-Host $errMsg
    exit 1
}